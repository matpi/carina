FROM quay.io/pypa/manylinux_2_28_x86_64

RUN dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo \
    && dnf -y update \
    && dnf -y install docker-ce --nobest \
    && dnf clean all \
    && rm -rf /var/cache/dnf \
    && pipx install hatch twine conan cibuildwheel~=2.17.0
