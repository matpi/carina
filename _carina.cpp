// SPDX-FileCopyrightText: 2024-present Quentin Wenger <matpi@protonmail.ch>
//
// SPDX-License-Identifier: GPL-3.0-or-later
#include <algorithm>
#include <map>
#include <cmath>
#include <iterator>
#include <set>
#include <sstream>
#include <utility>
#include <stack>
#include <tuple>
#include <vector>

#include <CGAL/Arr_batched_point_location.h>
#include <CGAL/Boolean_set_operations_2.h>
#include <CGAL/Exact_predicates_exact_constructions_kernel_with_sqrt.h>
#include <CGAL/IO/WKT.h>
#include <CGAL/Polygon_with_holes_2.h>
#include <CGAL/create_straight_skeleton_from_polygon_with_holes_2.h>
#include <CGAL/Segment_Delaunay_graph_2.h>
#include <CGAL/Segment_Delaunay_graph_adaptation_policies_2.h>
#include <CGAL/Segment_Delaunay_graph_adaptation_traits_2.h>
#include <CGAL/Segment_Delaunay_graph_traits_2.h>
#include <CGAL/Voronoi_diagram_2.h>

#include <Python.h>


using K = CGAL::Exact_predicates_exact_constructions_kernel_with_sqrt;
using Point = K::Point_2;
using Line = K::Line_2;
using Vector = K::Vector_2;
using Polygon = CGAL::Polygon_2<K>;
using Polygon_with_holes = CGAL::Polygon_with_holes_2<K>;

using MA_Gt = CGAL::Segment_Delaunay_graph_traits_2<K>;
using MA_SDG2 = CGAL::Segment_Delaunay_graph_2<MA_Gt>;
using MA_AT = CGAL::Segment_Delaunay_graph_adaptation_traits_2<MA_SDG2>;
using MA_AP = CGAL::Segment_Delaunay_graph_degeneracy_removal_policy_2<MA_SDG2>;
using MA_VoronoiDiagram = CGAL::Voronoi_diagram_2<MA_SDG2, MA_AT, MA_AP>;
using MA_Site = MA_AT::Site_2;
using MA_Halfedge = MA_VoronoiDiagram::Halfedge;
using LineString = std::vector<Point>;
using MultiLineString = std::vector<LineString>;

using Traits = CGAL::Arr_non_caching_segment_traits_2<K>;
using Segment = Traits::X_monotone_curve_2;
using Arrangement = CGAL::Arrangement_2<Traits>;
using Face_handle = Arrangement::Face_const_handle;
using Halfedge_handle = Arrangement::Halfedge_const_handle;
using Vertex_handle = Arrangement::Vertex_const_handle;
using Face_handles = std::set<Face_handle>;
using Point_location_result = CGAL::Arr_point_location_result<Arrangement>;


static void add_to_mls(MultiLineString &mls, const Point &pt1, const Point &pt2) {
	auto x1 = CGAL::to_double(pt1.x());
	auto y1 = CGAL::to_double(pt1.y());
	auto x2 = CGAL::to_double(pt2.x());
	auto y2 = CGAL::to_double(pt2.y());
	if (x1 > x2 || (x1 == x2 && y1 > y2)) {
		mls.push_back(LineString{{x2, y2}, {x1, y1}});
	}
	else {
		mls.push_back(LineString{{x1, y1}, {x2, y2}});
	}
}

static void sort_mls(MultiLineString &mls) {
	std::sort(mls.begin(), mls.end(), [](LineString &l1, LineString &l2) {
		double v1[] = {
			CGAL::to_double(l1[0].x()),
			CGAL::to_double(l1[0].y()),
			CGAL::to_double(l1[1].x()),
			CGAL::to_double(l1[1].y())
		};
		double v2[] = {
			CGAL::to_double(l2[0].x()),
			CGAL::to_double(l2[0].y()),
			CGAL::to_double(l2[1].x()),
			CGAL::to_double(l2[1].y())
		};
		for (int i = 0; i < 4; ++i) {
			if (v1[i] < v2[i]) {
				return true;
			}
			else if (v1[i] > v2[i]) {
				return false;
			}
		}
		return false;
	});
}

static Point compute_parabola_intersection_from_focal(Point focal, Line base, Line focal_line) {
	const auto perp = focal_line.perpendicular(focal);
	const auto nbase = base.has_on_negative_side(focal) ? base.opposite() : base;
	const auto res = CGAL::intersection(CGAL::bisector(perp, nbase), focal_line);
	return *boost::get<Point>(&*res);
}

static Point compute_parabola_intersection_from_base(Point focal, Line base, Point base_point, Line base_line) {
	const auto res = CGAL::intersection(CGAL::bisector(focal, base_point), base_line);
	return *boost::get<Point>(&*res);
}

static std::vector<Point> make_parabola_segments(Point pt1, Point pt2, Point focal, Line base, int number) {
	std::vector<Point> pts;
	pts.reserve(number + 1);
	pts.push_back(pt1);
	for (auto i = 1; i < number; ++i) {
		const auto pt = pt1 + Vector(pt1, pt2)*i/number;
		pts.push_back(compute_parabola_intersection_from_focal(focal, base, Line{focal, pt}));
	}
	pts.push_back(pt2);
	return pts;
}

static void segmentize_parabola(MultiLineString &mls, Point pt1, Point pt2, Point up, Line down, int reflex_parts, double reflex_angle) {
	if (reflex_parts == 1) {
		add_to_mls(mls, pt1, pt2);
		return;
	}
	if (reflex_parts == 0) {
		const auto v1 = Vector{up, pt1};
		const auto v2 = Vector{up, pt2};
		const K::FT angle = CGAL::approximate_angle(CGAL::Vector_3<K>{v1.x(), v1.y(), 0.0}, CGAL::Vector_3<K>{v2.x(), v2.y(), 0.0});
		reflex_parts = std::max(1, int(std::ceil(CGAL::to_double(angle/reflex_angle))));
	}
	const auto pts = make_parabola_segments(pt1, pt2, up, down, reflex_parts);
	for (auto i = 0; i < reflex_parts; ++i) {
		add_to_mls(mls, pts[i], pts[i + 1]);
	}
}

static Point find_poly_point_from_right_edge(const MA_Halfedge &he) {
	const auto su = he.up()->site();
	const auto sd = he.down()->site();
	Point ptu1;
	Point ptu2;
	if (su.is_point()) {
		ptu1 = su.point();
		ptu2 = su.point();
	}
	else {
		ptu1 = su.source();
		ptu2 = su.target();
	}
	Point ptd1;
	Point ptd2;
	if (sd.is_point()) {
		ptd1 = sd.point();
		ptd2 = sd.point();
	}
	else {
		ptd1 = sd.source();
		ptd2 = sd.target();
	}
	if (ptu1 == ptd1 || ptu1 == ptd2) {
		return ptu1;
	}
	else {
		return ptu2;
	}
}

enum PointPolyRelation {
	INSIDE = 0,
	ON_BOUNDARY,
	OUTSIDE,
	AT_INFINITE,
};

static void filter_voronoi_diagram_to_medial_axis(MultiLineString &mls, const MA_VoronoiDiagram &vd, const Face_handles &face_handles, const Arrangement &arrangement, int reflex_edges, int reflex_parts, double reflex_angle) {
	std::vector<Point> pts;
	pts.reserve(vd.number_of_vertices());
	for (auto vertex_iter = vd.vertices_begin(); vertex_iter != vd.vertices_end(); ++vertex_iter) {
		pts.push_back(vertex_iter->point());
	}
	std::map<Point, Point_location_result::Type> results;
	CGAL::locate(arrangement, pts.begin(), pts.end(), std::inserter(results, results.end()));
	std::map<Point, PointPolyRelation> orientations;
	for (const auto &result: results) {
		orientations[result.first] = PointPolyRelation::OUTSIDE;
		switch (result.second.which()) {
		case 0:
			{
				auto vh = *boost::get<Vertex_handle>(&result.second);
				auto circ = vh->incident_halfedges();
				auto heit = circ;
				do {
					if (face_handles.count(heit->face()) > 0) {
						orientations[result.first] = PointPolyRelation::ON_BOUNDARY;
					}
				}
				while (++heit != circ);
				break;
			}
		case 1:
			{
				auto hh = *boost::get<Halfedge_handle>(&result.second);
				if (face_handles.count(hh->face()) > 0 || face_handles.count(hh->twin()->face()) > 0) {
					orientations[result.first] = PointPolyRelation::ON_BOUNDARY;
				}
				break;
			}
		default:
		case 2:
			{
				auto fh = *boost::get<Face_handle>(&result.second);
				if (face_handles.count(fh) > 0) {
					orientations[result.first] = PointPolyRelation::INSIDE;
				}
			}
		}
	}

	std::stack<std::tuple<Point, Point, MA_Halfedge, bool>> remaining_segments;

	for (auto edge_iter = vd.edges_begin(); edge_iter != vd.edges_end(); ++edge_iter) {
		MA_Halfedge &halfedge = *edge_iter;
		auto rel1 = PointPolyRelation::AT_INFINITE;
		if (halfedge.has_source()) {
			rel1 = orientations.at(halfedge.source()->point());
		}
		auto rel2 = PointPolyRelation::AT_INFINITE;
		if (halfedge.has_target()) {
			rel2 = orientations.at(halfedge.target()->point());
		}
		if (rel1 > rel2) {
			halfedge = *halfedge.twin();
			std::swap(rel1, rel2);
		}
		if (rel1 >= PointPolyRelation::ON_BOUNDARY) {
			continue;
		}
		const auto &v1p = halfedge.source();
		if (rel2 >= PointPolyRelation::OUTSIDE) {
			if (reflex_edges > 0) {
				add_to_mls(mls, v1p->point(), find_poly_point_from_right_edge(halfedge));
			}
			continue;
		}
		const auto &v2p = halfedge.target();
		auto s1 = halfedge.up()->site();
		auto s2 = halfedge.down()->site();
		if (rel2 == PointPolyRelation::ON_BOUNDARY) {
			if ((s1.is_segment() && s2.is_segment()) || reflex_edges >= 2) {
				add_to_mls(mls, v1p->point(), v2p->point());
			}
			continue;
		}
		if (s1.is_segment() && s2.is_segment()) {
			add_to_mls(mls, v1p->point(), v2p->point());
			continue;
		}
		if (s1.is_segment() && s2.is_point()) {
			halfedge = *halfedge.twin();
			std::swap(s1, s2);
		}
		if (reflex_edges % 2 == 1) {
			remaining_segments.push({halfedge.source()->point(), halfedge.target()->point(), halfedge, true});
		}
		else {
			if (s2.is_segment()) {
				segmentize_parabola(mls, v1p->point(), v2p->point(), s1.point(), s2.segment().supporting_line(), reflex_parts, reflex_angle);
			}
			else {
				add_to_mls(mls, v1p->point(), v2p->point());
			}
		}
	}

	while (!remaining_segments.empty()) {
		Point pt1;
		Point pt2;
		MA_Halfedge he;
		bool is_first;
		std::tie(pt1, pt2, he, is_first) = remaining_segments.top();
		remaining_segments.pop();
		const auto ptu = he.up()->site().point();
		auto he2 = he;
		while (he2.target()->point() != ptu) {
			he2 = *he2.next();
		}
		const auto li = CGAL::bisector(Line{ptu, he2.source()->point()}, Line{ptu, he2.next()->target()->point()});
		const auto pt1_ori = li.oriented_side(pt1);
		const auto pt2_ori = li.oriented_side(pt2);
		if (
			(pt1_ori == CGAL::ON_NEGATIVE_SIDE && pt2_ori == CGAL::ON_POSITIVE_SIDE)
			|| (pt1_ori == CGAL::ON_POSITIVE_SIDE && pt2_ori == CGAL::ON_NEGATIVE_SIDE)
		) {
			if (he.down()->site().is_point()) {
				const auto res = CGAL::intersection(li, Line{pt1, pt2});
				const auto pt = *boost::get<Point>(&*res);
				add_to_mls(mls, ptu, pt);
				if (is_first) {
					remaining_segments.push({pt2, pt, *he.twin(), false});
					remaining_segments.push({pt, pt1, *he.twin(), false});
				}
				else {
					add_to_mls(mls, pt1, pt);
					add_to_mls(mls, pt, pt2);
				}
			}
			else {
				const auto ld = he.down()->site().segment().supporting_line();
				const auto pt = compute_parabola_intersection_from_focal(ptu, ld, li);
				add_to_mls(mls, ptu, pt);
				segmentize_parabola(mls, pt1, pt, ptu, ld, reflex_parts, reflex_angle);
				segmentize_parabola(mls, pt, pt2, ptu, ld, reflex_parts, reflex_angle);
			}
		}
		else {
			if (pt1_ori == CGAL::ON_ORIENTED_BOUNDARY && he.previous()->source()->point() != ptu) {
				add_to_mls(mls, ptu, pt1);
			}
			if (he.down()->site().is_point()) {
				if (is_first) {
					remaining_segments.push({pt2, pt1, *he.twin(), false});
				}
				else {
					add_to_mls(mls, pt1, pt2);
				}
			}
			else {
				const auto ld = he.down()->site().segment().supporting_line();
				segmentize_parabola(mls, pt1, pt2, ptu, ld, reflex_parts, reflex_angle);
			}
		}
	}
}

static bool arrangement_from_wkt(Arrangement &arrangement, PyObject *py_poly) {
	const char *wkt = PyUnicode_AsUTF8(py_poly);
	if (wkt == NULL) {
		return false;
	}
	std::stringstream ss;
	ss << wkt;
	std::string line;
	while (std::getline(ss, line)) {
		std::istringstream iss(line);
		std::istringstream jss(line);
		std::string trimmed_line;
		jss >> trimmed_line;
		std::vector<Polygon_with_holes> polys;
		if (trimmed_line.substr(0, 7).compare("POLYGON") == 0) {
			Polygon_with_holes poly;
			if (!CGAL::IO::read_polygon_WKT(iss, poly)) {
				PyErr_SetString(PyExc_ValueError, "Failed to parse WKT");
				return false;
			}
			polys.push_back(poly);
		}
		else if (trimmed_line.substr(0, 12).compare("MULTIPOLYGON") == 0) {
			if (!CGAL::IO::read_multi_polygon_WKT(iss, polys)) {
				PyErr_SetString(PyExc_ValueError, "Failed to parse WKT");
				return false;
			}
		}
		for (const auto &poly: polys) {
			const auto add_segments_to_arr = [&](const auto &loop) {
				for (const auto &edge: loop.edges()) {
					CGAL::insert(arrangement, edge);
				}
			};
			add_segments_to_arr(poly.outer_boundary());
			for (const auto &hole: poly.holes()) {
				add_segments_to_arr(hole);
			}
		}
	}
	return true;
}

static bool arrangement_from_iterable(Arrangement &arrangement, PyObject *py_poly) {
	PyObject *py_poly_iter = PyObject_GetIter(py_poly);
	if (py_poly_iter == NULL) {
		PyErr_SetString(PyExc_TypeError, "Polygon is not iterable");
		return false;
	}
	while (true) {
		std::vector<Point> pts;
		PyObject *py_loop = PyIter_Next(py_poly_iter);
		if (py_loop == NULL) {
			if (PyErr_Occurred()) {
				Py_DECREF(py_poly_iter);
				return false;
			}
			break;
		}
		PyObject *py_loop_iter = PyObject_GetIter(py_loop);
		if (py_loop_iter == NULL) {
			Py_DECREF(py_poly_iter);
			Py_DECREF(py_loop);
			PyErr_SetString(PyExc_TypeError, "Polygon loop is not iterable");
			return false;
		}
		while (true) {
			PyObject *py_point = PyIter_Next(py_loop_iter);
			if (py_point == NULL) {
				if (PyErr_Occurred()) {
					Py_DECREF(py_poly_iter);
					Py_DECREF(py_loop);
					Py_DECREF(py_loop_iter);
					return false;
				}
				break;
			}
			double x;
			double y;
			if (!PyArg_Parse(py_point, "(dd)", &x, &y)) {
				Py_DECREF(py_poly_iter);
				Py_DECREF(py_loop);
				Py_DECREF(py_loop_iter);
				Py_DECREF(py_point);
				PyErr_SetString(PyExc_TypeError, "Polygon loop item doesn't contain two doubles");
				return false;
			}
			Py_DECREF(py_point);
			pts.push_back({x, y});
		}
		Py_DECREF(py_loop);
		Py_DECREF(py_loop_iter);
		const auto n = pts.size();
		for (auto i = 0; i < n; ++i) {
			CGAL::insert(arrangement, Segment(pts[i], pts[(i + 1) % n]));
		}
	}
	Py_DECREF(py_poly_iter);
	return true;
}

static bool arrangement_from_py(Arrangement &arrangement, PyObject *py_poly, bool &from_wkt) {
	if (PyUnicode_Check(py_poly)) {
		from_wkt = true;
		return arrangement_from_wkt(arrangement, py_poly);
	}
	else {
		from_wkt = false;
		return arrangement_from_iterable(arrangement, py_poly);
	}
}

static void face_handles_from_arrangement(Face_handles &face_handles, const Arrangement &arrangement) {
	std::map<Face_handle, bool> side;
	std::deque<Face_handle> current_faces;
	const auto visit_face = [&](const auto &begin, const auto &end, const auto &value) {
		for (auto hit = begin; hit != end; ++hit) {
			const Arrangement::Ccb_halfedge_const_circulator circ = *hit;
			auto heit = circ;
			do {
				auto fp = heit->twin()->face();
				if (!fp->is_unbounded()) {
					auto res = side.emplace(fp, value);
					if (res.second) {
						if (value) {
							face_handles.emplace(fp);
						}
						current_faces.push_back(fp);
					}
				}
			}
			while (++heit != circ);
		}
	};
	auto fh = arrangement.unbounded_face();
	visit_face(fh->inner_ccbs_begin(), fh->inner_ccbs_end(), true);
	while (!current_faces.empty()) {
		auto fh = current_faces.front();
		current_faces.pop_front();
		visit_face(fh->inner_ccbs_begin(), fh->inner_ccbs_end(), !side[fh]);
		visit_face(fh->outer_ccbs_begin(), fh->outer_ccbs_end(), !side[fh]);
	}
}

static Polygon_with_holes poly_from_face_handle(const Face_handle &face_handle) {
	Polygon outer;
	const auto circ = face_handle->outer_ccb();
	auto heit = circ;
	do {
		outer.push_back(heit->source()->point());
	}
	while (++heit != circ);
	std::vector<Polygon> loops;
	for (auto hit = face_handle->inner_ccbs_begin(); hit != face_handle->inner_ccbs_end(); ++hit) {
		Polygon loop;
		const Arrangement::Ccb_halfedge_const_circulator circ = *hit;
		auto heit = circ;
		do {
			loop.push_back(heit->source()->point());
		}
		while (++heit != circ);
		loops.push_back(loop);
	}
	return Polygon_with_holes(outer, loops.cbegin(), loops.cend());
}

static void voronoi_diagram_from_face_handles(MA_VoronoiDiagram &vd, const Face_handles &face_handles) {
	const auto add_segments_to_vd = [&](const auto &ccb) {
		const Arrangement::Ccb_halfedge_const_circulator circ = ccb;
		auto heit = circ;
		do {
			vd.insert(MA_Site::construct_site_2(heit->source()->point(), heit->target()->point()));
		}
		while (++heit != circ);
	};
	for (const auto &face_handle: face_handles) {
		add_segments_to_vd(face_handle->outer_ccb());
		for (auto hit = face_handle->inner_ccbs_begin(); hit != face_handle->inner_ccbs_end(); ++hit) {
			add_segments_to_vd(*hit);
		}
	}
}

static PyObject *wkt_from_mls(MultiLineString &mls) {
	std::stringstream ss;
	CGAL::IO::write_multi_linestring_WKT(ss, mls);
	const std::string str = ss.str();
	return PyUnicode_FromString(str.c_str());
}

/*
static PyObject *tuple_from_mls(MultiLineString &mls) {
	std::map<Point, std::tuple<double, double>> pts_as_doubles;
	for (const auto &ls: mls) {
		pts_as_doubles[ls[0]] = {CGAL::to_double(ls[0].x()), CGAL::to_double(ls[0].y())};
		pts_as_doubles[ls[1]] = {CGAL::to_double(ls[1].x()), CGAL::to_double(ls[1].y())};
	}
	PyObject *py_mls = PyTuple_New(mls.size());
	if (py_mls == NULL) {
		return NULL;
	}
	Py_ssize_t pos = 0;
	for (const auto &ls: mls) {
		const auto &doubles_1 = pts_as_doubles[ls[0]];
		const auto &doubles_2 = pts_as_doubles[ls[1]];
		PyObject *py_ls = Py_BuildValue("((dd)(dd))", std::get<0>(doubles_1), std::get<1>(doubles_1), std::get<0>(doubles_2), std::get<1>(doubles_2));
		if (py_ls == NULL) {
			Py_DECREF(py_mls);
			return NULL;
		}
		PyTuple_SET_ITEM(py_mls, pos, py_ls);
		++pos;
	}
	return py_mls;
}
*/

static PyObject *tuple_from_mls(MultiLineString &mls) {
	PyObject *py_mls = PyTuple_New(mls.size());
	if (py_mls == NULL) {
		return NULL;
	}
	Py_ssize_t pos = 0;
	for (const auto &ls: mls) {
		PyObject *py_ls = Py_BuildValue("((dd)(dd))", CGAL::to_double(ls[0].x()), CGAL::to_double(ls[0].y()), CGAL::to_double(ls[1].x()), CGAL::to_double(ls[1].y()));
		if (py_ls == NULL) {
			Py_DECREF(py_mls);
			return NULL;
		}
		PyTuple_SET_ITEM(py_mls, pos, py_ls);
		++pos;
	}
	return py_mls;
}

static PyObject *py_from_mls(MultiLineString &mls, bool to_wkt) {
	if (to_wkt) {
		return wkt_from_mls(mls);
	}
	else {
		return tuple_from_mls(mls);
	}
}


static PyObject *carina_medial_axis(PyObject *self, PyObject *args) {
	PyObject *py_poly;
	int validate_vd;
	unsigned int reflex_edges;
	unsigned int reflex_parts;
	double reflex_angle;
	if (!PyArg_ParseTuple(args, "Opiid", &py_poly, &validate_vd, &reflex_edges, &reflex_parts, &reflex_angle)) {
		return NULL;
	}
	if (reflex_edges < 0 || reflex_edges > 3) {
		PyErr_SetString(PyExc_ValueError, "Invalid reflex edges number");
		return NULL;
	}
	if (reflex_parts < 0) {
		PyErr_SetString(PyExc_ValueError, "Invalid reflex parts number");
		return NULL;
	}
	if (reflex_parts == 0 && reflex_angle <= 0.0) {
		PyErr_SetString(PyExc_ValueError, "Invalid reflex angle");
		return NULL;
	}
	Arrangement arrangement;
	bool from_wkt;
	if (!arrangement_from_py(arrangement, py_poly, from_wkt)) {
		return NULL;
	}

	Face_handles face_handles;
	MA_VoronoiDiagram vd;

	Py_BEGIN_ALLOW_THREADS
	face_handles_from_arrangement(face_handles, arrangement);
	voronoi_diagram_from_face_handles(vd, face_handles);
	Py_END_ALLOW_THREADS

	if (validate_vd == 1 && !vd.is_valid()) {
		PyErr_SetString(PyExc_RuntimeError, "Invalid Voronoi diagram");
		return NULL;
	}

	MultiLineString mls;

	Py_BEGIN_ALLOW_THREADS
	filter_voronoi_diagram_to_medial_axis(mls, vd, face_handles, arrangement, reflex_edges, reflex_parts, reflex_angle);
	sort_mls(mls);
	Py_END_ALLOW_THREADS

	return py_from_mls(mls, from_wkt);
}

static PyObject *carina_straight_skeleton(PyObject *self, PyObject *args) {
	PyObject *py_poly;
	if (!PyArg_ParseTuple(args, "O", &py_poly)) {
		return NULL;
	}
	Arrangement arrangement;
	bool from_wkt;
	if (!arrangement_from_py(arrangement, py_poly, from_wkt)) {
		return NULL;
	}

	MultiLineString mls;

	Py_BEGIN_ALLOW_THREADS
	Face_handles face_handles;
	face_handles_from_arrangement(face_handles, arrangement);
	for (const auto &face_handle: face_handles) {
		const auto poly = poly_from_face_handle(face_handle);
		auto iss = CGAL::create_interior_straight_skeleton_2(poly);
		for (auto hit = iss->halfedges_begin(); hit != iss->halfedges_end(); ++hit) {
			if (hit->is_bisector() && ((hit->id() % 2) == 0) && !hit->has_infinite_time() && !hit->opposite()->has_infinite_time()) {
				add_to_mls(mls, Point{hit->vertex()->point().x(), hit->vertex()->point().y()}, Point{hit->opposite()->vertex()->point().x(), hit->opposite()->vertex()->point().y()});
			}
		}
	}
	sort_mls(mls);
	Py_END_ALLOW_THREADS

	return py_from_mls(mls, from_wkt);
}

static PyMethodDef carina_methods[] = {
	{"medial_axis", carina_medial_axis, METH_VARARGS, NULL},
	{"straight_skeleton", carina_straight_skeleton, METH_VARARGS, NULL},
	{NULL} /* Sentinel */
};

static struct PyModuleDef carina_module = {
	PyModuleDef_HEAD_INIT,
	"_carina",
	NULL,
	-1,
	carina_methods,
};

PyMODINIT_FUNC PyInit__carina(void) {
	PyObject *module = PyModule_Create(&carina_module);
	if (module == NULL) {
		return NULL;
	}
	return module;
}
