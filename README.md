# Carina: skeletons from CGAL

[![PyPI - Version](https://img.shields.io/pypi/v/carina.svg)](https://pypi.org/project/carina)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/carina.svg)](https://pypi.org/project/carina)

This package provides functions to build straight skeletons and medial axes of polygons, with an implementation based on CGAL[1].

IO accepts nested tuples and WKT.

The behavior of flat/concave vertices and parabolic arcs discretization in medial axes can be controlled.

The code for medial axes takes inspiration from an answer by Richard Barnes on StackOverflow[2].

## Building

Building requires the development files of CGAL, including the GMP and MPFR libraries installed.

## Origin of the name

The carina (or *keel*) is one of the most central and distinctive bones in avian skeletons, hence also in seagulls. It is crucial for flight.

## License

`carina` is distributed under the terms of the [GNU General Public License v3.0 or later](https://spdx.org/licenses/GPL-3.0-or-later.html) license.

## Acknowledgements

[1] CGAL, The Computational Geometry Algorithms Library, <https://www.cgal.org/>

[2] RE: "How do you get the medial axis of a multipolygon using CGAL?", <https://stackoverflow.com/a/69237155>
