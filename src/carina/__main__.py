# SPDX-FileCopyrightText: 2024-present Quentin Wenger <matpi@protonmail.ch>
#
# SPDX-License-Identifier: GPL-3.0-or-later
import argparse
import sys

import carina


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Compute a polygon skeleton, in WKT representation")
    subparsers = parser.add_subparsers()
    parser_medial = subparsers.add_parser("medial", help="Compute a medial axis.")
    parser_medial.add_argument("-n", "--no-poly-validation", action="store_false", dest="validate_poly", help="Do not validate the input.")
    parser_medial.add_argument("-e", "--reflex-edges", type=int, default=1, help="Number of edges to use for reflex vertices.")
    parser_medial.add_argument("-p", "--reflex-parts", type=int, default=1, help="Number of parts to reflex parabola interpolation.")
    parser_medial.add_argument("-a", "--reflex-angle", type=float, default=0.0, help="Mean angle for reflex parabola interpolation.")
    parser_medial.add_argument("-d", "--vd-validation", action="store_true", dest="validate_vd", help="Validate the intermediate Voronoi diagram.")
    parser_medial.set_defaults(func=carina.medial_axis)
    parser_straight = subparsers.add_parser("straight", help="Compute a straight skeleton.")
    parser_straight.add_argument("-n", "--no-poly-validation", action="store_false", dest="validate_poly", help="Do not validate the input.")
    parser_straight.set_defaults(func=carina.straight_skeleton)
    args = vars(parser.parse_args())
    func = args.pop("func")
    for line in sys.stdin.readlines():
        print(func(line, **args), end="")
