# SPDX-FileCopyrightText: 2024-present Quentin Wenger <matpi@protonmail.ch>
#
# SPDX-License-Identifier: GPL-3.0-or-later
from numbers import Real
from typing import Iterable
from typing import Tuple
from typing import Union
from warnings import warn

import _carina


__all__ = ["medial_axis", "straight_skeleton"]

Polygon = Iterable[Iterable[Tuple[Real, Real]]]
MultiLineString = Iterable[Tuple[Tuple[float, float], Tuple[float, float]]]

def _post_filter(result: MultiLineString) -> MultiLineString:
    segments = []
    for segment in sorted(result):
        if segment[0] == segment[1]:
            continue
        if segments and segments[-1] == segment:
            continue
        segments.append(segment)
    return tuple(segments)

def medial_axis(
    poly: Union[str, Polygon],
    post_filter: bool = False,
    fix_orientation = None,
    validate_poly = None,
    validate_vd: bool = False,
    reflex_edges: int = 1,
    reflex_parts: int = 1,
    reflex_angle: float = 0.0,
) -> Union[str, MultiLineString]:
    if fix_orientation is not None:
        warn(
            "The fix_orientation argument is now a noop and will be removed in a future release",
            DeprecationWarning,
            stacklevel=2,
        )
    if validate_poly is not None:
        warn(
            "The validate_poly argument is now a noop and will be removed in a future release",
            DeprecationWarning,
            stacklevel=2,
        )
    result = _carina.medial_axis(poly, validate_vd, reflex_edges, reflex_parts, reflex_angle)
    if post_filter and not isinstance(result, str):
        return _post_filter(result)
    else:
        return result

def straight_skeleton(
    poly: Union[str, Polygon],
    post_filter: bool = False,
    fix_orientation = None,
    validate_poly = None,
) -> Union[str, MultiLineString]:
    if fix_orientation is not None:
        warn(
            "The fix_orientation argument is now a noop and will be removed in a future release",
            DeprecationWarning,
            stacklevel=2,
        )
    if validate_poly is not None:
        warn(
            "The validate_poly argument is now a noop and will be removed in a future release",
            DeprecationWarning,
            stacklevel=2,
        )
    result = _carina.straight_skeleton(poly)
    if post_filter and not isinstance(result, str):
        return _post_filter(result)
    else:
        return result
