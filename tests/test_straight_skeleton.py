# SPDX-FileCopyrightText: 2024-present Quentin Wenger <matpi@protonmail.ch>
#
# SPDX-License-Identifier: GPL-3.0-or-later
import carina


def test_base():
    assert carina.straight_skeleton("""
        POLYGON((0.0 0.0,1.0 0.0,1.0 1.0,0.0 1.0))
    """).strip() == """
        MULTILINESTRING((0 0,0.5 0.5),(0 1,0.5 0.5),(0.5 0.5,1 0),(0.5 0.5,1 1))
    """.strip()

def test_flat():
    assert carina.straight_skeleton("""
        POLYGON((0.0 0.0,0.5 0.0,1.0 0.0,1.0 1.0,0.0 1.0))
    """).strip() == """
        MULTILINESTRING((0 0,0.5 0.5),(0 1,0.5 0.5),(0.5 0,0.5 0.5),(0.5 0.5,1 0),(0.5 0.5,1 1))
    """.strip()

def test_multi_polygon_wkt():
    assert carina.medial_axis("""
        MULTIPOLYGON(((0.0 0.0,1.0 0.0,1.0 1.0,0.0 1.0)),((1.0 0.0,2.0 0.0,2.0 1.0,1.0 1.0)))
    """).strip() == """
        MULTILINESTRING((0 0,0.5 0.5),(0 1,0.5 0.5),(0.5 0.5,1 0),(0.5 0.5,1 1),(1 0,1.5 0.5),(1 1,1.5 0.5),(1.5 0.5,2 0),(1.5 0.5,2 1))
    """.strip()

def test_overlapping_loops():
    assert carina.straight_skeleton(((
        (0.0, 0.0),
        (4.0, 0.0),
        (4.0, 4.0),
        (0.0, 4.0),
    ), (
        (2.0, 2.0),
        (6.0, 2.0),
        (6.0, 6.0),
        (2.0, 6.0),
    )), post_filter=True) == (
        ((0.0, 0.0), (1.0, 1.0)),
        ((0.0, 4.0), (1.0, 3.0)),
        ((1.0, 1.0), (1.0, 3.0)),
        ((1.0, 1.0), (2.0, 2.0)),
        ((1.0, 1.0), (3.0, 1.0)),
        ((1.0, 3.0), (2.0, 4.0)),
        ((2.0, 4.0), (3.0, 5.0)),
        ((2.0, 6.0), (3.0, 5.0)),
        ((3.0, 1.0), (4.0, 0.0)),
        ((3.0, 1.0), (4.0, 2.0)),
        ((3.0, 5.0), (5.0, 5.0)),
        ((4.0, 2.0), (5.0, 3.0)),
        ((4.0, 4.0), (5.0, 5.0)),
        ((5.0, 3.0), (5.0, 5.0)),
        ((5.0, 3.0), (6.0, 2.0)),
        ((5.0, 5.0), (6.0, 6.0)),
    )
